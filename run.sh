#!/usr/bin/env bash

OUT_DIR="out"
NUM_REDUCERS=4

hdfs dfs -rm -r -skipTrash $OUT_DIR.tmp > /dev/null

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="minecraft 1" \
    -D mapreduce.job.reduces=$NUM_REDUCERS \
    -files mapper.py,reducer.py \
    -mapper ./mapper.py \
    -reducer ./reducer.py \
    -input /data/minecraft-server-logs \
    -output ${OUT_DIR}/tmp > /dev/null

hdfs dfs -rm -r -skipTrash ${OUT_DIR} > /dev/null

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="minecraft 2" \
    -D mapreduce.job.reduces=1 \
    -D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
    -D mapred.text.key.comparator.options=-k2,1nr \
    -files mapper2.py,reducer2.py \
    -mapper ./mapper2.py \
    -reducer ./reducer2.py \
    -input ${OUT_DIR}/tmp \
    -output ${OUT_DIR} > /dev/null

hdfs dfs -cat ${OUT_DIR}/part-00000 | head
