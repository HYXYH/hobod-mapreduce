#!/usr/bin/env python

import sys


sessions = 0
commands = 0
current_user = None
session_is_open = False

for line in sys.stdin:
    try:
        username, msg_type = line.strip().split('\t', 2)
    except ValueError as e:
        continue

    if current_user != username:
        if current_user:
            if !session_is_open:
                sessions += 1
            print "%s\t%d\t%d" % (current_user, commands / sessions, sessions)
        sessions = 0.0
        commands = 0.0
        current_user = username
        session_is_open = False

    if 'command' in msg_type and session_is_open:
        commands += 1
    elif 'close' in msg_type:
        session_is_open = False
    elif 'open' in msg_type:
        sessions += 1
        session_is_open = True

if !session_is_open:
    sessions += 1
print "%s\t%d\t%d" % (current_user, commands / sessions, sessions)