#!/usr/bin/env python

import sys


for line in sys.stdin:
    try:
        info, msg, _ = line.strip().split(']:', 2)
        if 'UUID of player' in msg:
            username = msg.replace('UUID of player', '').split(' ')[0]
            print "%s\topen" % (username)
            
        elif 'issued server command:' in msg:
            username = msg.split(' ')[0]
            print "%s\tcommand" % (username)

        elif 'com.mojang.authlib.GameProfile' in msg and 'lost connection' in msg:
            for part in msg.split(','):
                if 'name=' in part:
                    username = msg.replace('name=', '')
                    print "%s\tclose" % (username)

        elif 'lost connection: Disconnected' in msg:
            username = msg.split(' ')[0]
            print "%s\tclose" % (username)


    except ValueError as e:
        continue